@echo off
rem -- Check if argument is INSTALL or REMOVE

if not ""%1"" == ""INSTALL"" goto remove

"C:/wappstack/apache2\bin\httpd.exe" -k install -n "wappstackApache" -f "C:/wappstack/apache2\conf\httpd.conf"

net start wappstackApache >NUL
goto end

:remove
rem -- STOP SERVICE BEFORE REMOVING

net stop wappstackApache >NUL
"C:/wappstack/apache2\bin\httpd.exe" -k uninstall -n "wappstackApache"

:end
exit
