@echo off
rem -- Check if argument is INSTALL or REMOVE

if not ""%1"" == ""INSTALL"" goto remove

"C:\wappstack/postgresql\bin\pg_ctl.exe" register -N "wappstackPostgreSQL" -D "C:\wappstack/postgresql/data"

net start "wappstackPostgreSQL" >NUL
goto end

:remove
rem -- STOP SERVICE BEFORE REMOVING

net stop "wappstackPostgreSQL" >NUL
"C:\wappstack/postgresql\bin\pg_ctl.exe" unregister -N "wappstackPostgreSQL"


:end
exit
